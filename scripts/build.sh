#!bin/bash

cp target/*.jar .
docker build --build-arg VERSION=$VERSION -t $DOCKER_IMAGE .
docker push $DOCKER_IMAGE
docker container prune -f
docker rmi -f $DOCKER_IMAGE

